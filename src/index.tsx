export type { ColorfulButtonProps } from './components/colorful-button';
export { default as ColorfulButton } from './components/colorful-button';

export type { ColorfulInputProps } from './components/colorful-input';
export { default as ColorfulInput } from './components/colorful-input';

export type {SohoDivProps} from './components/soho-div'
export {default as SohoDiv} from './components/soho-div'

const bizCssPrefix = 'bizpack';

export {
  bizCssPrefix
}
