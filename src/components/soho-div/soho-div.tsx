import * as React from 'react';
import { createElement } from 'react';
import { Button } from '@alifd/next';
import './index.scss';

export interface SohoDivProps {
  /**
   * 类型
   */
  type?: "primary" | "secondary" | "normal";
  /**
   * 颜色
   */
  color?: string;
  style?: object;
}

const SohoDiv: React.FC<SohoDivProps> = function ColorfulButton({
  type = 'primary',
  color,
  style = {},
  ...otherProps
}) {
  const _style = style || {};
  if (color) {
    _style.backgroundColor = color;
  }
  const _otherProps = otherProps || {};
  _otherProps.style = _style;
  return (
    <div type={type} { ..._otherProps } >测试中</div>
  );
};

SohoDiv.displayName = 'DIV';
export default SohoDiv;


